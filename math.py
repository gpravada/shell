def add(a, b):
    return a+b

def sub(a, b):
    return a-b

def mul(a, b):
    return a*b

def div(a, b):
    return a/b

def Or(a, b):
    return a | b

def And(a, b):
    return a & b

def Xor(a, b):
    return a ^ b

print(add(1,2))
print(sub(3,2))
print(mul(1,2))
print(div(4,2))
print(Or(1,2))
print(And(4,2))
print(Xor(1,2))
